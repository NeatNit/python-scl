import scl

def test(test_input):
  print(">|"+test_input+"|<")
  print("\n".join("\t"+str(t) for t in scl.parse(test_input)))

test("hello  'world'   ")
test("   stgg h b'jlpi\"fts'fy h\"kl feh f\".")
test("h\"kl feh f\"j ")
