# Copyright © 2023 Nitai Sasson ניתאי ששון
# https://codeberg.org/NeatNit/python-scl
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program. If not, see: https://www.gnu.org/licenses/

from enum import Enum, auto

# class CharacterClass(Enum):
#   WHITESPACE    = auto()  #  Space, tab, newline, ...?
#   PLAIN         = auto()  # Characters with no special meaning
#   SINGLE_QUOTE  = auto()  # "
#   DOUBLE_QUOTE  = auto()  # '
#   SPECIAL_IN_DOUBLE_QUOTE = auto()  # $, `, ...?
#   SPECIAL       = auto()  # (), <>, *, ~, ...?

# CHAR_CATEGORY = {char: charclass for (charclass, string) in {
#   # TODO: this is not complete or correct (yet)!
#   CharacterClass.SINGLE_QUOTE: "'",
#   CharacterClass.DOUBLE_QUOTE: '"',
#   CharacterClass.WHITESPACE: " \t\n",
#   CharacterClass.SPECIAL_IN_DOUBLE_QUOTE: "$`\\",
#   CharacterClass.SPECIAL: "()<>*~"
#   }.items() for char in string}

# def categorize(char):
#   assert len(char) == 1
#   return CHAR_CATEGORY.get(char, CharacterClass.PLAIN)

BLANK_CHARS = " \t" # NOTE: in C++, the correct test is iswblank() from <cwctype>

class ParseState(Enum):
  BLANK    = auto()
  PLAIN         = auto()
  SINGLE_QUOTE  = auto()
  DOUBLE_QUOTE  = auto()
  # add more obviously

class TokenType(Enum):
  LITERAL             = auto()
  REPARSE_RECURSIVELY = auto()  # anything that MAY be split into multiple tokens after rules are applied, e.g. "$@", $*, `echo "hi there   hello"`
  UNFINISHED          = auto() # end of input in illegal state
  # add more, e.g. subshell (``, ${}), escaped builtin command (\ls? is that a thing?)

def parse(command):
  tokens_tree = []
  state_stack = [(ParseState.BLANK, tokens_tree)]
  
  active_token = ""
  active_token_type = TokenType.LITERAL
  for char in command:
    #category = categorize(char)
    (state, active_token_tree) = state_stack[-1]
    #print(f"{char}: {category} {state} {tokens_tree}")
    match (state, char):
      case (ParseState.BLANK|ParseState.PLAIN, '"'):
        state_stack[-1] = (ParseState.DOUBLE_QUOTE, active_token_tree)
      case (ParseState.DOUBLE_QUOTE, '"'):
        state_stack[-1] = (ParseState.PLAIN, active_token_tree)
        
      case (ParseState.BLANK|ParseState.PLAIN, "'"):
        state_stack[-1] = (ParseState.SINGLE_QUOTE, active_token_tree)
      case (ParseState.SINGLE_QUOTE, "'"):
        state_stack[-1] = (ParseState.PLAIN, active_token_tree)
      
      case (ParseState.BLANK, c) if c in BLANK_CHARS:
        pass
      case (ParseState.PLAIN, c) if c in BLANK_CHARS:
        active_token_tree.append((active_token_type, active_token))
        active_token = ""
        state_stack[-1] = (ParseState.BLANK, active_token_tree)
      case _:
        # This is the simplest case - add a character to the current token
        active_token += char
        if state == ParseState.BLANK:
          # Start a plain token
          state_stack[-1] = (ParseState.PLAIN, active_token_tree)
 
  match state_stack[-1][0]:
    case ParseState.PLAIN:
      # Finish active token
      active_token_tree.append((active_token_type, active_token))
    case ParseState.BLANK:
      pass
    case state:
      active_token_tree.append((TokenType.UNFINISHED, (state_stack[-1], active_token)))
  return tokens_tree
